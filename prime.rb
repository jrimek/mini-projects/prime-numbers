
class Prime
	def self.validate_input(script_param)
		if script_param.nil? == true
			#calc_primes(10)
			[true, 10]
		elsif script_param == "0"
			[false, "ERROR: the number has to be greater than 0"]
		elsif script_param.to_i == 0
			[false, "ERROR: param has to be a number"]
		else
			[true, script_param.to_i]
		end
	end

	def self.calc_primes(amount)
		max_number = 1000

		#Checking if the the range from 2..max_number contains enough prime numbers
		#using the Prime Number Theorem
		while (max_number / Math.log(max_number, Math::E) * 1.2) < amount
			max_number = max_number * 10
		end

		numbers =* (0..max_number)		

		#finding all prime numbers using Sieve of Eratosthenes
		(2..max_number/2).each do |i|
			j = 2
			while i*j <= max_number do
				numbers[i*j] = 0
				j += 1	
			end
		end

		result = []
		numbers.each do |number|
			result << number if number.to_i > 1
		end

		result[0..amount]
	end
end

checked_input = Prime.validate_input(ARGV[0])
	
if checked_input[0] == true
	puts Prime.calc_primes(checked_input[1])
else
	puts checked_input[1]
end