require 'spec_helper'
 
describe Prime do

	before :each do
	    #
	end
 
 	describe ".validate_input" do
 	    it "puts out 10 prime numbers, if the given parameter is nil" do
 	    	expect(Prime.validate_input(nil)[0]).to eq(true)
 	    	expect(Prime.validate_input(nil)[1]).to eq(10)
 	    end 
 	    it "puts out the amount of prime numbers given as an argument" do
 	        expect(Prime.validate_input(15)[1]).to eq(15)
 	        expect(Prime.validate_input(15)[0]).to eq(true)

 	        expect(Prime.validate_input(5)[1]).to eq(5)
 	        expect(Prime.validate_input(5)[0]).to eq(true)
 	    end
 	    it "puts out an error message if the argument is 0" do
 	        expect(Prime.validate_input("0")[1]).to eq("ERROR: the number has to be greater than 0")
 	        expect(Prime.validate_input("0")[0]).to eq(false)
 	    end
		it "puts out an error message if the argument is not a number" do
 	        expect(Prime.validate_input("test")[1]).to eq("ERROR: param has to be a number")
 	        expect(Prime.validate_input("test")[0]).to eq(false)
 	    end
 	end

 	describe ".calc_primes" do
 		it "should put out the correct first 10 prime numbers" do
 			expect(Prime.calc_primes(10)).to match_array([2,3,5,7,11,13,17,19,23,29,31])
 		end
 	end
end